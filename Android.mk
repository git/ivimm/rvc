LOCAL_PATH:= $(call my-dir)

#
# appA15HeartBeatHost
#

##### appA15HeartBeatHost #####
include $(CLEAR_VARS)

IPC_ROOT := ../ipc/

LOCAL_C_INCLUDES +=  $(LOCAL_PATH)/$(IPC_ROOT)/linux/include \
                     $(LOCAL_PATH)/$(IPC_ROOT)/packages \
                     $(LOCAL_PATH)/hlos_common/include

LOCAL_CFLAGS += -DIPC_BUILDOS_ANDROID

LOCAL_MODULE_TAGS:= optional

LOCAL_SRC_FILES:= appA15HeartBeatHost.c

LOCAL_SHARED_LIBRARIES := \
    liblog libtiipcutils libtiipc libtitransportrpmsg

LOCAL_MODULE:= appA15HeartBeatHost
include $(BUILD_EXECUTABLE)
