/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== appA15HeartBeatHost.h ========
 *
 */

#ifndef appA15HeartBeatHost__include
#define appA15HeartBeatHost__include
#if defined (__cplusplus)
extern "C" {
#endif

#include <utils/Log.h>
#define LOG_TAG "AppA15HeartBeatHost"

/* notify commands 00 - FF */
#define App_CMD_MASK            0xFF000000
#define App_CMD_START           0x00000000
#define App_CMD_ALIVE           0x01000000
#define App_CMD_SHUTDOWN        0x02000000
#define App_CMD_RVC_UP          0x01001000
#define App_CMD_RVC_DOWN        0x01002000

#define App_MsgHeapId           0
#define App_HostMsgQueName      "HOST:MsgQ:01"
#define App_SlaveMsgQueName     "%s:MsgQ:01"  /* %s is each slave's Proc Name */

#define Main_USAGE "\
Usage:\n\
    appA15HeartBeatHost [options] procName\n\
\n\
Arguments:\n\
    procName      : the name of the remote processor\n\
\n\
Options:\n\
    h   : print this help message\n\
    l   : list the available remote names\n\
\n\
Examples:\n\
    appA15HeartBeatHost DSP\n\
    appA15HeartBeatHost -l\n\
    appA15HeartBeatHost -h\n\
\n"

//#define DEBUG

#define Microseconds (500 * 1000)

Int App_create(UInt16 remoteProcId);
Int App_delete();
Int App_exec();


#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */
#endif /* appA15HeartBeatHost__include */
