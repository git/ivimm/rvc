/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== appA15HeartBeatHost.c ========
 *
 */

/* host header files */
#include <stdio.h>
#include <unistd.h>
#include <time.h>

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/transports/TransportRpmsg.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>

/* private functions */
static Int Main_main(Void);
static Int Main_parseArgs(Int argc, Char *argv[]);

/* local header files */
#include "appA15HeartBeatHost.h"

/* module structure */
typedef struct {
    MessageQ_MsgHeader  reserved;
    UInt32              cmd;
} App_Msg;

typedef struct {
    MessageQ_Handle         hostQue;    // created locally
    MessageQ_QueueId        slaveQue;   // opened remotely
    UInt16                  heapId;     // MessageQ heapId
    UInt32                  msgSize;
} App_Module;

/* private data */
static App_Module Module;
static String   Main_remoteProcName = NULL;
#ifdef DEBUG
static UInt32 MessageCount = 0;
#endif
/*
 *  ======== App_create ========
 */

Int App_create(UInt16 remoteProcId)
{
    Int                 status = 0;
    MessageQ_Params     msgqParams;
    char                msgqName[32];

    ALOGE("--> App_create:");

    /* setting default values */
    Module.hostQue = NULL;
    Module.slaveQue = MessageQ_INVALIDMESSAGEQ;
    Module.heapId = App_MsgHeapId;
    Module.msgSize = sizeof(App_Msg);

    /* create local message queue (inbound messages) */
    MessageQ_Params_init(&msgqParams);

    Module.hostQue = MessageQ_create(App_HostMsgQueName, &msgqParams);

    if (Module.hostQue == NULL) {
        ALOGE("App_create: Failed creating MessageQ");
        status = -1;
        goto leave;
    }

    /* open the remote message queue */
    sprintf(msgqName, App_SlaveMsgQueName, MultiProc_getName(remoteProcId));
    ALOGE("App_create: msgqName %s", msgqName);
    do {
        status = MessageQ_open(msgqName, &Module.slaveQue);
        sleep(1);
    } while (status == MessageQ_E_NOTFOUND);

    if (status < 0) {
        ALOGE("App_create: Failed opening MessageQ");
        goto leave;
    }

    ALOGE("App_create: Host is ready");

leave:
    ALOGE("<-- App_create:");
    return(status);
}


/*
 *  ======== App_delete ========
 */
Int App_delete(Void)
{
    Int         status;

    ALOGE("--> App_delete:");

    /* close remote resources */
    status = MessageQ_close(&Module.slaveQue);

    if (status < 0) {
        goto leave;
    }

    /* delete the host message queue */
    status = MessageQ_delete(&Module.hostQue);

    if (status < 0) {
        goto leave;
    }

leave:
    ALOGE("<-- App_delete:");
    return(status);
}


/*
 *  ======== App_exec ========
 */
Int App_exec(Void)
{
    Int         status;
    Int         i;
    App_Msg *   msg;
    Bool        running = TRUE;
    struct timeval tv;

    ALOGE("--> App_exec:");

    /* allocate message */
    msg = (App_Msg *)MessageQ_alloc(Module.heapId, Module.msgSize);

    if (msg == NULL) {
        status = -1;
        goto leave;
    }

    /* fill in message payload */
    msg->cmd = App_CMD_START;
#ifdef DEBUG
    MessageCount++;
    // Get current time of day
    gettimeofday(&tv, NULL);
    ALOGE("%ld us: App_exec: sending #%d message %d \n",
        (tv.tv_sec * 1000000) + tv.tv_usec, MessageCount, msg->cmd);
#endif

    while (running) {
        /* send START 1st message */
        MessageQ_put(Module.slaveQue, (MessageQ_Msg)msg);

        /* Wait for server message within 500ms */
        status = MessageQ_get(Module.hostQue, (MessageQ_Msg *)&msg,
            Microseconds);
        /* Check the message from server that indicates GPIO reverse gear
         * detected or non-reverse gear detected.
         * If timeout, then reset the 500ms sleep and resend the A15 heartbeat
         * to server.
         * If reverse gear is detected, then set the A15 watchdog/panic
         * timeout to 0 sec (no recovery/reboot should take place).
         * If non-reverse is detected, then set the A15 watchdog/panic
         * timout to 5 sec.
         */
        if ((status < 0) && (status != MessageQ_E_TIMEOUT)) {
            ALOGE("--> App_exec: MessageQ_get FAILED");
            status = -1;
            goto leave;
        }

        if (status == MessageQ_S_SUCCESS) {
            /* Received a message from server
             * Extract message payload: check reverse or non-reverse indication
             */
            if (msg->cmd == App_CMD_RVC_UP) {
                ALOGE("--> App_exec: Message received App_CMD_RVC_UP");
                system("echo 0 > /proc/sys/kernel/panic");
            }
            else if (msg->cmd == App_CMD_RVC_DOWN) {
                ALOGE("--> App_exec: Message received App_CMD_RVC_DOWN");
                system("echo 5 > /proc/sys/kernel/panic");
            }

            /* Free the message from M4 -
             * msg to indicate rvc reverse gear detected or not
             */
            MessageQ_free((MessageQ_Msg)msg);
        }

        if (status == MessageQ_E_TIMEOUT) {
            /* Receive a TIMEOUT. No message from M4, continue heartbeat
             */
            //ALOGE("App_exec: message TIMEOUT, continue heartbeat \n");
        }

        /* Allocate message
         */
        msg = (App_Msg *)MessageQ_alloc(Module.heapId, Module.msgSize);

        if (msg == NULL) {
            ALOGE("--> App_exec: MessageQ_alloc FAILED");
            status = -1;
            goto leave;
        }

        /* Fill in message payload */
        msg->cmd = App_CMD_ALIVE;
#ifdef DEBUG
        MessageCount++;
        gettimeofday(&tv, NULL);
        ALOGE("%ld us: App_exec: sending #%d App_CMD_ALIVE \n",
            (tv.tv_sec * 1000000) + tv.tv_usec, MessageCount);
#endif
    }

leave:
    ALOGE("<-- App_exec: %d\n", status);
    return(status);
}

/*
 *  ======== main ========
 */
Int main(Int argc, Char* argv[])
{
    Int status;

    ALOGE("--> main:");

    /* Configure the transport factory */
    Ipc_transportConfig(&TransportRpmsg_Factory);

    /* Parse command line */
    status = Main_parseArgs(argc, argv);

    if (status < 0) {
        goto leave;
    }

    /* Ipc initialization */
    status = Ipc_start();

    if (status >= 0) {
        /* Application create, exec, delete */
        status = Main_main();

        /* Ipc finalization */
        Ipc_stop();
    }
    else {
        ALOGE("Ipc_start failed: status = %d\n", status);
        goto leave;
    }

leave:
    ALOGE("<-- main:\n");
    status = (status >= 0 ? 0 : status);

    return (status);
}


/*
 *  ======== Main_main ========
 */
Int Main_main(Void)
{
    UInt16      remoteProcId;
    Int         status = 0;

    ALOGE("--> Main_main:\n");

    remoteProcId = MultiProc_getId(Main_remoteProcName);

    /* application create phase */
    status = App_create(remoteProcId);

    if (status < 0) {
        goto leave;
    }

    /* application execute phase */
    status = App_exec();

    if (status < 0) {
        goto leave;
    }

    /* application delete phase */
    status = App_delete();

    if (status < 0) {
        goto leave;
    }

leave:
    ALOGE("<-- Main_main:");

    status = (status >= 0 ? 0 : status);
    return (status);
}


/*
 *  ======== Main_parseArgs ========
 */
Int Main_parseArgs(Int argc, Char *argv[])
{
    Int             x, cp, opt, argNum;
    UInt16          i, numProcs;
    String          name;
    Int             status = 0;


    /* parse the command line options */
    for (opt = 1; (opt < argc) && (argv[opt][0] == '-'); opt++) {
        for (x = 0, cp = 1; argv[opt][cp] != '\0'; cp++) {
            x = (x << 8) | (int)argv[opt][cp];
        }

        switch (x) {
            case 'h': /* -h */
                ALOGE("%s", Main_USAGE);
                exit(0);
                break;

            case 'l': /* -l */
                //ALOGE("Processor List");
                printf("Processor List\n");
                status = Ipc_start();
                if (status >= 0) {
                    numProcs = MultiProc_getNumProcessors();
                    for (i = 0; i < numProcs; i++) {
                        name = MultiProc_getName(i);
                        //ALOGE("    procId=%d, procName=%s", i, name);
                        printf("    procId=%d, procName=%s\n", i, name);
                    }
                    Ipc_stop();
                }
                else {
                    printf(
                        "Error: %s, line %d: Ipc_start failed status %d\n",
                        __FILE__, __LINE__, status);
                    goto leave;
                }
                exit(0);
                break;

            default:
                printf(
                    "Error: %s, line %d: invalid option, %c\n",
                    __FILE__, __LINE__, (Char)x);
                printf("%s", Main_USAGE);
                status = -1;
                goto leave;
        }
    }

    /* parse the command line arguments */
    for (argNum = 1; opt < argc; argNum++, opt++) {

        switch (argNum) {
            case 1: /* name of proc #1 */
                Main_remoteProcName = argv[opt];
                break;

            default:
                printf(
                    "Error: %s, line %d: too many arguments\n",
                    __FILE__, __LINE__);
                printf("%s", Main_USAGE);
                status = -1;
                goto leave;
        }
    }

    /* validate command line arguments */
    if (Main_remoteProcName == NULL) {
        printf("Error: missing procName argument\n");
        printf("%s", Main_USAGE);
        status = -1;
        goto leave;
    }

leave:
    return(status);
}
